"use strict";

const backendAddress = "https://sportstoday-api-k3s-prod.mendonca.xyz";
const modal = document.querySelector(".modal");
const overlay = document.querySelector(".overlay");
const btnCloseModal = document.querySelector(".close-modal");
const btnsShowModal = document.querySelectorAll(".show-modal");
const btnDropdown = document.querySelector(".btn-dropdown");
const inputs = document.querySelectorAll("input");
const btnLogIn = document.querySelector("#btn-login");

// Form validation
const patterns = {
  email: /^([a-z\d\.-]+)@([a-z\d-]+)\.([a-z]{2,8})(\.[a-z]{2,8})?$/,
  password: /^[\w@-]{8,20}$/,
};

// Validation function
function validate(field, regex) {
  //console.log(regex.test(field.value));
  if (regex.test(field.value)) {
    field.className = "valid";
  } else {
    field.className = "invalid";
  }
}

inputs.forEach((input) => {
  input.addEventListener("keyup", (e) => {
    //console.log(e.target.attributes.name.value);
    validate(e.target, patterns[e.target.attributes.name.value]);
  });
});

// Modal
const closeModal = function () {
  modal.classList.add("hidden");
  overlay.classList.add("hidden");
};

const openModal = function () {
  modal.classList.remove("hidden");
  overlay.classList.remove("hidden");
};

for (let i = 0; i < btnsShowModal.length; i++) {
  //console.log(btnsShowModal);
  btnsShowModal[i].addEventListener("click", openModal);

  btnCloseModal.addEventListener("click", closeModal);
  overlay.addEventListener("click", closeModal);
}

document.addEventListener("keydown", function (event) {
  //console.log(event.key);

  if (event.key === "Escape" && !modal.classList.contains("hidden")) {
    closeModal();
  }
});

// submit form
btnLogIn.addEventListener("click", function formSubmit(e) {
  e.preventDefault();
  inputs.forEach((input) => {
    console.log(input.value);
  });
});

// Highlights
fetch(`${backendAddress}/highlights`)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    displayHighligths(data);

    function displayHighligths(data) {
      const highlights = document.querySelector(".highLights-container");

      for (let i = 0; i < data.length - 1; i++) {
        let highlight = `
          <div class="highlights">
            <img class="highLights-img" src="${backendAddress}${data[i].img}"/>
            <p class="credits">${data[i].credit}</p>
            <p>${data[i].text}</p>
          </div>`;
        highlights.innerHTML += highlight;
      }

      let lastHighlight = `
        <div class="highlights" id="highlight-hidden">
          <img class="highLights-img" src="${backendAddress}${
        data[data.length - 1].img
      }"/>
          <p class="credits">${data[data.length - 1].credit}</p>
          <p>${data[data.length - 1].text}</p>
        </div>`;
      highlights.innerHTML += lastHighlight;
    }
  })
  .catch((err) => {
    console.log("rejected", err);
  });

// Featured Players

fetch(`${backendAddress}/players`)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    displayPlayers(data);

    function displayPlayers(data) {
      const featuredPlayers = document.querySelector(".players-container");

      for (let i = 0; i < data.length; i++) {
        let player = `
                    <div class="player">
                        <img class="photo" src="${backendAddress}${data[i].picture}" />
                        <h3 class="name">${data[i].name}</h3>
                        <p class="sport">${data[i].sport.name}</p>
                        <img class="flag" src="${backendAddress}${data[i].country.flag}"/>
                        <p class="country-code">${data[i].country.code}</p>
                    </div>`;
        featuredPlayers.innerHTML += player;
      }
    }
  })
  .catch((err) => {
    console.log("rejected", err);
  });

// Medals

fetch(`${backendAddress}/countries`)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    buildTable(data);

    function buildTable(data) {
      const countriesTable = document.getElementById("countries-table");

      for (let i = 0; i < data.length; i++) {
        let row = `<tr>
            <td>${data[i].ranking}</td>
            <td>${data[i].name}</td>
            <td class="gold">${data[i].gold_medals}</td>
            <td class="silver">${data[i].silver_medals}</td>
            <td class="bronze">${data[i].bronze_medals}</td>
            <td>${data[i].total_medals}</td>
        </tr>`;
        countriesTable.innerHTML += row;
      }
    }
  })
  .catch((err) => {
    console.log("rejected", err);
  });

// //Dropdow menu

btnDropdown.addEventListener("click", dropdown);

function dropdown() {
  document.querySelector("#dropdown").classList.toggle("show");
}
