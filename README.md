# Sports Today

## Description

The Sports Today project was developed as a Frontend exercise. It simulates a responsive webpage for a Sports broadcaster company.

## About the Project

This project follows the basic structure of a standard Sports company webpage. It contains a header, a menu, sections of content and a footer. To a achieve this result all the markup, stylling and funcionality were created from scratch using HTML, CSS, and Javascript.

## Next steps

Because Sports Today was a first Frontend solo project, there are many Design and code improvements to be made. Some of the changes that will be implemented in the near future are:

- Improvement of the CSS and Javascript codes for screens under 500px.
- Creation of a dropdown menu for mobile devices.
- Implementation of animations and transition properties to enhance the user experience.
- Add a sign-up field to the navigation.
